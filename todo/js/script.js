let task1 = { title: "Putzen", who: "ich", done: true };
let task2 = { title: "Müll", who: "Patrick", done: false };

let tasks = [];

showList();

function showList() {
    let html = "";
    if (tasks.length === 0)
    {
        html +=
        "<li class='task'>"
            +"<div class='tasktitle'>"
                +"vvv You can add tasks down here. vvv"
            +"</div>"
        +"</li>";
    }
    else
    {
        for (let i = 0; i < tasks.length; i++)
        {
            let checked;
            if (tasks[i].done) {checked = "checked";}
            else {checked = "unchecked";}
            html +=
            "<li class='task'>"
                +"<input type='checkbox' onclick='toggleChecked(this)' data-index='"+ i +"' "+ checked +"/>"
                +"<div class='tasktitle'>"
                    +tasks[i].title
                +"</div>"
                +"<div class='taskwho'>"
                    +tasks[i].who
                +"</div>"
            +"</li>";

        }
    }
    document.getElementById("task-list").innerHTML = html;
    document.getElementById("txt-tasktitle").focus();
}

document.getElementById("btn-addtask").addEventListener("click", function () {
    let tasktitle = document.getElementById("txt-tasktitle").value;
    let taskwho = document.getElementById("txt-taskwho").value;
    if (tasktitle == "") {tasktitle = "Kein Titel";}
    let task = { title: tasktitle , who: taskwho, done: false };
    tasks.push(task);

    /*document.getElementsById("taskcheck" + (tasks.length-1).toString()).addEventListener("click", function () {
        tasks[tasks.length-1].done = !tasks[tasks.length-1].done;
        showList();
    });*/

    document.getElementById("txt-tasktitle").value = "";
    document.getElementById("txt-taskwho").value = "";
    showList();
});

function toggleChecked(element) {
    tasks[element.attributes["data-index"].value].done = element.checked;
    document.getElementById("txt-tasktitle").focus();
}